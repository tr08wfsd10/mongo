1.Obtener todos los documentos
> db.movie.find();

2.Obtener documentos con writer igual a "Quentin Tarantino"
> db.movie.find({writer:"Quentin Tarantino"});

3.Obtener documentos con actors que incluyan a "Brad Pitt"
> db.movie.find({actors:"Brad Pitt"});

4. Obtener documentos con franchise igual a "The Hobbit"
> db.movie.find({franchise:"The Hobbit"});

5. Obtener todas las películas de los 90s.
> db.movie.find({$and:[{year:{$gt:1990},year:{$lt:2000}}]});

6. Obtener las películas estrenadas entre el año 2000 y 2010
> db.movie.find({$and:[{year:{$gt:2000,$lt:2010}}]});

1.Agregar sinopsis a "The Hobbit: An Unexpected Journey" : "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home and the gold within it from the dragon Smaug."
>  db.movie.updateOne({title:"The Hobbit: An Unexpected Journey"},{$set:{synopsis:"A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home and the gold within it from the dragon Smaug."}});

2.Agregar sinopsis a "The Hobbit: The Desolation of Smaug" : "The dwarves, along with
Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring."
>  db.movie.updateOne({title:"The Hobbit: The Desolation of Smaug"},{$set:{synopsis:"The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring."}});

3. Agregar una actor llamado "Samuel L. Jackson" a la película "Pulp Fiction"
db.movie.update({title:"Pulp Fiction"},{$push:{actors : "Samuel L. Jackson"}});

1.Encontrar las películas que en el título contenga la palabra "Hobbit"
> db.movie.find({title:{$regex:/Hobbit/ }});

2.Encontrar las películas que en la sinopsis contengan la palabra "Gandalf"
db.movie.find({synopsis:{$regex:"Gandalf"}});

3.Encontrar las películas que en la sinopsis contengan la palabra "Bilbo" y no la palabra "Gandalf"
>  db.movie.find({$and:[{synopsis:/Bilbo/},{synopsis:{$not:/Gandalf/}}]});

